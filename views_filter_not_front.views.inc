<?php

/**
 * Implements hook_views_data().
 */
function views_filter_not_front_views_data() {
  $data = [];
  $data['node_field_data']['not_front_filter'] = [
    'title' => t('Not front page'),
    'help' => t('if front page is a node'),
    'filter' => [
      'field' => 'nid',
      'id' => 'not_front'
    ]
  ];
  return $data;
}