## Exclude Frontpage Node Views filter

This module add a new views filter, that allows you to exclude the frontpage
node from views.
**New**: A Search API processor plugin that allows you to exclude the frontpage
node from Search API indexes

It checks if system.site.page.front is a node and if thats true,
this node will be excluded from the views results.

This can be helpful if you have changing front page nodes and do always
want to exclude the front page node from certain views. While you can exclude
the frontpage node also by using the builtin node id filter from views,
you would have to manually update the node id filter
if you change the frontpage.
This module is also useful if you do not know what the front page node
will be, for example if you install a view from configuration
in different sites and each site can possibly have different front page nodes.
If the frontpage is not a node, the filter will be skipped.

Installation:

    Install module as asual.
    Add the filter "Not front page" to your view.
    optional add Search API processor "Exclude front page node" to your index

