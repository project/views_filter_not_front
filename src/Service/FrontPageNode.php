<?php

namespace Drupal\views_filter_not_front\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Router;
use Drupal\node\Entity\Node;

/**
 * Class FrontPageNode
 *
 * get frontpage node if front page is a node
 *
 * @package Drupal\views_filter_not_front\Service
 */
class FrontPageNode {
  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * @var \Drupal\Core\Routing\Router
   */
  protected $router;

  /**
   * @var array|mixed|null
   */
  protected $frontpageUrl;

  /**
   * @var \Drupal\node\Entity\Node|false
   */
  protected $frontpageNode;

  /**
   * FrontPageNode constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Routing\Router $router
   */
  public function __construct(ConfigFactoryInterface $config_factory, Router $router) {
    $this->config = $config_factory;
    $this->router = $router;
    $this->frontpageUrl = $this->config->get('system.site')->get('page.front');
    try {
      $route = $this->router->match($this->frontpageUrl);
      if (isset($route['node']) && $route['node'] instanceof Node) {
        $this->frontpageNode = $route['node'];
      }
      else {
        $this->frontpageNode = FALSE;
      }
    }
    catch (\Exception $e) {
      $this->frontpageNode = FALSE;
    }

  }

  /**
   * front page node getter
   * @return \Drupal\node\Entity\Node | false
   */
  public function getFrontpageNode() {
    return $this->frontpageNode;
  }
}