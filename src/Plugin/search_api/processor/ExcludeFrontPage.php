<?php

namespace Drupal\views_filter_not_front\Plugin\search_api\processor;

use Drupal\node\NodeInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\views_filter_not_front\Service\FrontPageNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes front page node from node indexes.
 *
 * @SearchApiProcessor(
 *   id = "exclude_front_page_node",
 *   label = @Translation("Exclude front page node"),
 *   description = @Translation("Exclude front page node from being indexed."),
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class ExcludeFrontPage extends ProcessorPluginBase {

  /**
   * @var FrontPageNode service
   */
  protected $notFrontService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->setNotFrontService($container->get('views_filter_not_front.frontpage_node'));

    return $processor;
  }

  /**
   * @param \Drupal\views_filter_not_front\Service\FrontPageNode $not_front_service
   */
  public function setNotFrontService(FrontPageNode $not_front_service) {
    $this->notFrontService = $not_front_service;
  }

  /**
   * @return FrontPageNode
   */
  public function getNotFrontService() {
    return $this->notFrontService ?: \Drupal::service('views_filter_not_front.frontpage_node');
  }

  /**
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    foreach ($index->getDatasources() as $datasource) {
      $entity_type_id = $datasource->getEntityTypeId();
      if (!$entity_type_id) {
        continue;
      }
      if ($entity_type_id === 'node') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    // Annoyingly, this doc comment is needed for PHPStorm. See
    // http://youtrack.jetbrains.com/issue/WI-23586
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $object = $item->getOriginalObject()->getValue();
      if ($object instanceof NodeInterface) {
        if ($front_page_node = $this->getNotFrontService()->getFrontpageNode()) {
          if ($object->id() == $front_page_node->id()) {
            unset($items[$item_id]);
          }
        }
      }
    }
  }

}
