<?php

namespace Drupal\views_filter_not_front\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views_filter_not_front\Service\FrontPageNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Exclude front page if it is a node
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("not_front")
 */
class NotFront extends FilterPluginBase {

  /**
   * @var \Drupal\node\Entity\Node|false
   */
  protected $frontpageNode;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FrontPageNode $frontpage_node) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->frontpageNode = $frontpage_node->getFrontpageNode();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('views_filter_not_front.frontpage_node')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->value = FALSE;
    $this->operator = "<>";
    $this->no_operator = TRUE;

    if ($this->frontpageNode) {
      $this->value = $this->frontpageNode->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->value) {
      parent::query();
    }
  }

}